
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>       
#include <sys/socket.h>
#include <string.h> //memset
#include <netinet/ip.h> // sockaddr

int main(int argc, char* argv[])
{
    int server_fd;
    int client_fd;
    struct sockaddr_in sa;
    
    server_fd = socket(PF_INET, SOCK_STREAM, 0);
    
    if (server_fd == -1)
    {
        perror("Cannot create server socket\n");
        exit(1);
    }

    printf("Server socket file descriptor was %d\n", server_fd);
    
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(42121);
    sa.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if(bind(server_fd, (struct sockaddr*)&sa, sizeof(sa)) == -1)
    {
        perror("Cannot bind socket\n");
        exit(1);
    }
    
    printf("Socket bound\n");
    
    if(listen(server_fd, 5) == -1)
    {
        perror("Listen failed\n");
        exit(1);
    }
    
    printf("Listen success\n");
    
    client_fd = accept(server_fd, NULL, NULL);
    
    if(client_fd == -1)
    {
        perror("Accept failed\n");
        exit(1);
    }
    
    printf("Accept succeded - we can talk to the client on file descritor %d\n", client_fd);
    
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>       
#include <sys/socket.h>

int main(int argc, char* argv[])
{
    int server_fd;
    
    server_fd = socket(PF_INET, SOCK_STREAM, 0);
    
    if (server_fd == -1)
    {
        perror("Cannot create server socket\n");
        exit(1);
    }

    printf("Server socket file descriptor was %d\n", server_fd);
    
    return 0;
}
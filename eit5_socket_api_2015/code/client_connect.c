
#include <stdio.h>
#include <stdlib.h> 
#include <sys/types.h>       
#include <sys/socket.h>
#include <string.h> //memset
#include <netinet/ip.h> // sockaddr

int main(int argc, char* argv[])
{
    int socket_fd;
    struct sockaddr_in sa;
    
    socket_fd = socket(PF_INET, SOCK_STREAM, 0);
    
    if (socket_fd == -1)
    {
        perror("Cannot create socket");
        exit(1);
    }
    printf("Socket descriptor was %d\n", socket_fd);
    
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(42121);
    
    // Set the server IP
    if(inet_pton(AF_INET, "127.0.0.1", &sa.sin_addr) == -1)
    {
        perror("inet_pton failed\n");
        exit(1);
    }
    
    if(connect(socket_fd, (struct sockaddr*)&sa, sizeof(sa)) == -1)
    {
        perror("connect failed\n");
        exit(1);
    }
    
    printf("connect success\n");
     
    return 0;
}